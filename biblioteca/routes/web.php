<?php
/*

*/

Route::get('/', 'HomeController@getHome');

Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/logout', function () {
    return "Logout usuario";
});
Route::get('/catalog', 'CatalogController@getIndex');
Route::get('/catalog/show/{id}', 'CatalogController@getShow');
Route::get('/catalog/create', 'CatalogController@getCreate');
Route::post('/catalog/create', 'CatalogController@getCreate');
Route::get('/catalog/edit/{id}', 'CatalogController@getEdit');

Route::get('/author', 'AuthorController@getIndex');
Route::get('/author/show/{id}', 'AuthorController@getShow');
Route::get('/author/create', 'AuthorController@getCreate');
Route::get('/author/edit/{id}', 'AuthorController@getEdit');

Route::get('/editorial', 'EditorialController@getIndex');
Route::get('/editorial/show/{id}', 'EditorialController@getShow');
Route::get('/editorial/create', 'EditorialController@getCreate');
Route::get('/editorial/edit/{id}', 'EditorialController@getEdit');


Route::get('/home/cat', function () {
    return "hola món";
});

Route::get('/home/en', function () {
    return "hello world";
});

Route::match(array('GET', 'POST'), '/home/cat/show/{nom?}', function($nom = 'desconegut') {
	return '¡Hola '.$nom.'!';
})->where('nom', '[A-Za-z]+');

