<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

@extends('layouts.master')

@section('content')
   
   Catalogo de libros
   <div class="row">
       
   @foreach( $libros as $libro )
   <div class="col-xs-6 col-sm-4 col-md-3 text-center">

   <a href="{{ url('/catalog/show/' . $libro->id ) }}">
           <h4 style="min-height:45px;margin:5px 0 10px 0">
           {{$libro->title}}
           </h4>
   </a>
   </div>
   @endforeach
</div>
@stop