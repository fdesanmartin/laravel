<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

@extends('layouts.master')
@section('content')
<div class="row" style="margin-top:20px">
    <div class="col-md-offset-3 col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                        <span class="glyphicon glyphicon-film" aria-hidden="true"></span>
                        Añadir libro
                </h3>
            </div>
            <div class="panel-body" style="padding:30px">  
                <form action="{{ action('CatalogController@getCreate') }}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Título</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="year">Año</label>
                        <input type="text" name="year" id="year" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="escritor">Escritor</label>
                        <input type="text" name="escritor" id="escritor" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="synopsis">Resumen</label>
                        <textarea name="synopsis" id="synopsis" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Añadir libro
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
