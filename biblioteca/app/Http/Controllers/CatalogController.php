<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use App\model\librosModel;
use App\Libro;

class CatalogController extends Controller
{   
       
    public function getIndex(){
        //print_r($this->arrayLibros); die();
        return view('catalog.index', array('libros' => Libro::get()));
    }
    public function getShow($id){
        return view('catalog.show', array('libro' => Libro::findOrFail($id), 'id' => $id));
    }    
    public function getCreate(){
        return view('catalog.create');
    }
    public function getEdit($id){
        return view('catalog.edit', array('libro' => Libro::findOrFail($id)));
    }
}
